import pandas as pd
import os
import matplotlib.pyplot as plt
plt.rc('text', usetex=True)
plt.rc('font', family='serif')
plt.style.use('seaborn-ticks')
import sys
import numpy as np

PATH_TO_MARKOVID =os.getcwd()

output_colnames = ['t',
             'u',
             'bust',
             'Pavg',
             'Wavg',
             'S',
             'Atot',
             'firm-savings',
             'debt-tot',
             'inflation',
             'pi-avg',
             'propensity',
             'k',
             'Dtot',
             'rhom',
             'rho',
             'rhop',
             'pi-used',
             'tau-tar',
             'tau-meas',
             'R', 
            'Wtot',
            'etap-avg',
            'etam-avg', 
            'w-nominal',
            'Ytot',
             'deftot', 
            'profits',
            'debt-ratio',
            'firms-alive',
            'left',
            'u-af', 
            'bust-af', 
            'frag', 
            'true_end',
            'theta']
            


flagnames = ['base','cons_pure', 'cons_prod','cons_theta', 'prod_debt']
fnames = [os.path.join('output', flagname) for flagname in flagnames]
    
def read_output(shockflag, res_dir=PATH_TO_MARKOVID):
    '''
    Given a shockflag, reads the output of the simulation.
    Parameters:
    shockflag (int): Shockflag

    Returns:
    pandas.DataFrame: Returns the output as a pandas dataframe. 
    '''
    fname = fnames[shockflag]
    res = {}
    fname = os.path.join(res_dir, fname+'.txt')
    tmp = pd.read_csv(fname, sep = "\t", header = None, index_col = False)
    assert len(tmp.columns) == len(output_colnames)
    tmp.columns = output_colnames
    return tmp

def generate_param_list(param_names, param_values, base_string='./markovid'):
    '''
    Prepares the command line string with parameters to run program from command line. 

    Parameters:
    param_names  (list): Names of parameters that will be passed
    param_values (list): Values of the named parameters
    base_string  (str): Name of program to run (default: markovid)
    
    Returns:
    program_string (str): Program with command line parameters. 
    '''
    for param_name, param_value in zip(param_names, param_values):
        #change to format to ensure python<3.7 compatibility
        base_string += " --{}={} ".format(param_name, param_value)
        
#         base_string += f" --{param_name}={param_value} "
    return base_string


def run_program_default(param_names, param_values, program_name = './markovid'):
    '''
    Runs the markovid program on the command line. If the program is not found, 
    compiles it first and then runs it 
    
    Parameters:
    param_names  (list): Names of parameters that will be passed
    param_values (list): Values of the named parameters
    program_name  (str): Name of program to run (default: markovid)
    
    Returns:
    program_output: Return value from call to os.system
    '''
    program_string = generate_param_list(param_names, param_values, program_name)
    if (not os.path.isfile('markovid')):
        print("Compiling program")
        os.system('make markovid')
    if (not os.path.isdir('output')):
        print("Creating output folder")
        os.makedirs('output')
    print("Running program")
    print(program_string)
    program_output = os.system(program_string)
    return program_output

            
def plot_evolution(res, t_start, t_end, ):
    '''
    Plots the evolution of the economy after the shock
    
    Parameters:
    res (pandas.DataFrame): DataFrame with output from simulation
    t_start (int): Start of the shock 
    t_end (int): End of the shock
    
    Returns
    Generates the plot inline. 
    '''
    start=t_start-10
    end = t_start+120
    y = range(start-t_start, end-t_start)
    f, ax = plt.subplots(nrows = 2, ncols =3, figsize=(12, 4), dpi=200,)
    cols1 = ['Ytot', 'u', 'Pavg', 'Wavg']
    cols2 = ['bust-af', 'frag', 'S']
    colnames1 = ['Total output', 'U-rate', 'Inflation/Wages']
    colnames2 = ['Bankruptcies', 'Fragility', 'Savings']

    for i in range(2):
        ax[0,i].plot(y,res[cols1[i]][start:end])
        ax[0,i].set_title(colnames1[i])
    ax[0,-1].plot(y, res[cols1[-2]][start:end], label = "1+Inflation")
    ax[0,-1].plot(y, res[cols1[-1]][start:end], label = "Real Wages")
    ax[0,-1].legend()
    ax[0,-1].set_title(colnames1[-1])
    for i in range(3):
        ax[1,i].plot(y, res[cols2[i]][start:end])
        ax[1,i].set_title(colnames2[i])
    plt.tight_layout()
    
    for axis in ax.flatten():
        axis.axvspan(0,t_end-t_start, facecolor='0.5', alpha=0.5)
    plt.suptitle("Evolution after shock", fontsize=16)
    plt.subplots_adjust(top=0.85)
    

    
    
CFLAGS          = -O3 -pedantic --std=c++11
INCL            = -I/usr/include
COMP            = g++
CC		= gcc
%:%.cpp 
	$(COMP) $<  $(CFLAGS) $(INCL) -o $@

.PHONY : clean
clean:
	rm -f *.o markovid


#include <stdio.h>
#include <math.h>
#include <stdlib.h>
#include <time.h>
#include <stdbool.h>
#include <algorithm>
#include <random>
#include <numeric>

#include "utils.hpp"
using namespace std;

#define ZERO            1.e-10
#define DOANTICIPATE    true

char dividends = 'A';


double red(double x){ return ( fabs(x) < ZERO ? 0.0 : x);}

double compute_ema(double previous, double current,  double omega=0.2){
  return omega*current + (1-omega)*previous;
}

template<typename T, typename A>
T sum_vector(std::vector<T,A> const& data ){
    T sum = 0.0;

    for (typename std::vector<T,A>::const_iterator it = data.begin(); it != data.end(); ++it){
        sum += *it;
    }
    return sum;
}


int main (int argc, char *argv[ ]){

    FILE    *out;
    char    add[10000], name[10000];


    //variables
    double  bust, Pavg, Pold, u, S, Atot, firm_savings, debt_tot, Ytot, Wtot, e, Wavg, inflation, k, propensity, Dtot, rho;
    double  rhom, rhop, rp_avg, pi_avg, Gamma, u_avg, rm_avg;
    int     N;

    //parameters
    double   gammap, gammaw, theta, alpha_g, eta0m, eta0p, beta, R, r, alpha, rho0, f, alpha_e, alpha_pi, Gamma0, G0, phi, taupi, delta;

    //others;
    double  Pmin, rp, rw, ren, Pnorm, arg, pay_roll, dY, p , tmp, budget, interests;
    double  Wmax, wage_norm, u_share, deftot;
    int     i, t, seed, new_firm, new_len;
    double  profits;

    double pi_target, e_target; //target inflation and employment for CB
    int    negative_count=0;
    double tau_meas , tau_tar;
    double wage_factor;


    f = 0.5; // is this c_0
    beta = 2.0; // Price sensitivity parameter
    r = 1.0; 

    double y0 = 0.5;
                     //
    double cfactor = 0.0; // Factor by which consumption goes down during consumption shock.

    double zeta = 0.0; // Productivity factor
    double ptol;
    double zfactor;

    int helico;
    int extra_cons;
    int adapt;
    // Preparing the program

    //Parse the arguments passed to program

    Parameters program_params("covid", "Running covid shocks on Mark0");

    if (argc <2){
      std::cout << "Running with default options" << std::endl;
    }

    VectorPtr ptr_vec = {&R, &theta, &Gamma0, &rho0, &alpha, &alpha_pi, &alpha_e, &pi_target, &e_target, &tau_tar, &wage_factor,&y0, &gammap, &eta0m, &tau_meas, &alpha_g, &cfactor, &zeta, &zfactor, &ptol, &G0, &phi, &taupi, &delta};


    int shockflag, t_start, t_end, policy_start, policy_end, extra_steps, tsim, teq, tprint, renorm;
    std::vector<int * > ptr_vec_int = { &seed, &shockflag, &t_start, &t_end, &policy_start, &policy_end,&helico , &N, &extra_cons, &adapt, &extra_steps, &tsim, &teq, &tprint, &renorm} ;

    program_params.parse_cmdline(argc, argv, ptr_vec, ptr_vec_int);
    String path_name = "output/";
    ShockDetails shock_details;
    try{
        shock_details = ShockDetails(shockflag, t_start, t_end, tsim);

    } catch (const std::invalid_argument& e){
        std::cerr << "exception: " << e.what() << " Flag supplied was " << shockflag << std::endl;
        std::cout << "Accepted flags from 0-4" << std::endl;
        exit(1);
    }
    String s_add;
    if (program_params.outputname.empty()){
        s_add = shock_details.filename;
        std::cout << "Filename " << s_add << std::endl;
    }
    else{
      s_add = program_params.outputname;
    }


    String final_file_name = path_name + s_add;


    FileWriter output (final_file_name + ".txt");
    Vector output_vector;
    String underscore = std::string("_");


    Shocks debt_policy(policy_start, policy_end, tsim);
    if (shockflag <= 2 || shockflag > 4 ){
        debt_policy.no_shock();
    }
    //array
    Vector  P(N), Y(N), D(N), A(N), W(N), PROFITS(N);
    std::vector<int>     ALIVE(N), new_list(N);
    Vector etaplus(N), etaminus(N); // to store hiring and firing rates
    Vector wage_nominal(N);

    double R0 = R;
    eta0p  = R*eta0m;
    gammaw = r*gammap;
    double G = G0;



    printf("R = %.2e\nN = %d\ntheta = %.2e\ngp = %.2e\tgw = %.2e\nf = %.2e\nb = %.2e\nalphag = %.2e\nalpha=%f\n\n",R,N,theta,gammap,gammaw,f,beta,alpha_g,alpha);
    printf("rho0 = %.2e\tap = %.2e\tae = %.2e\n",rho0,alpha_pi,alpha_e);
    printf("pit = %.2e\tet = %.2e\n",pi_target,e_target);
    printf("taut = %.2e\ttaum = %.2e\n",tau_tar,tau_meas);
    printf("eta0m= %.2e\teta0p = %.2e\n",eta0m,eta0p);
    printf("seed %d\n",seed);

    char params[10000];
    sprintf(params,"%.2e %.2e %.2e %.2e %.2e %.2e %.2e %.2e %.2e %.2e %.2e %.2e",rho0,alpha_pi,alpha_e,pi_target,e_target,theta,R,alpha_g,Gamma0,alpha,tau_meas,tau_tar);



    int     collapse, avg_counter;

    double theta0 = theta;
    double zeta0 = zeta;
    /* ************************************** INIT ************************************** */

    if(seed==-1)
        seed = time( NULL );


    std::mt19937 gen(seed);
    std::uniform_real_distribution<double> dis(0.0, 1.0);

    Pavg = 0.;
    Ytot = 0.;
    Wtot = 0.;
    Atot = 0.;
    Wmax = Wavg = 1.;
    Pold = 1.;
    inflation = pi_avg = 0.;
    rhom = rho = rm_avg = rho0;
    rhop = rp_avg = 0.;
    u_avg = 0.;
     /* *********************************** INIT  ************************************ */
    for(i=0;i<N;i++){

        ALIVE[i] =  1;
        P[i]     =  1.  + 0.1*(2*dis(gen)-1.);
        Y[i]     =  y0  + 0.1*(2*dis(gen)-1.);
        Y[i]     *= zeta;
        D[i]     =  y0;
        W[i]     =  zeta;
        PROFITS[i] = P[i]*std::min(D[i],Y[i]) - W[i]*Y[i];
        wage_nominal[i] = W[i];

        A[i] =  2.0*Y[i]*W[i]*dis(gen); // cash balance

        Atot += A[i];
        Ytot += Y[i];
        Pavg += P[i]*Y[i];
        Wtot += Y[i]*W[i];

        etaplus[i] = 0.0;
        etaminus[i] = 0.0;

    }


    e = Ytot / N;
    e /= zeta;
    u = 1. - e ;

    Pavg /= Ytot;
    Wavg = Wtot/Ytot;

    S = e*N;
    //S = 0.0;
    double a0 = 0.0;
    //fix total amount of money to 0
    double M0 = a0*N;
    tmp = Atot + S;
    S = S*(N*a0)/tmp;
    Atot=0.;

    for(i=0;i<N;i++){
        A[i]  = A[i]*(N*a0)/tmp;
        Atot += A[i];
    }

    std::cout << "S = " << S << " Atot = " << Atot << std::endl;

    std::cout << "zeta " << zeta << " zfactor " << zfactor << " extra_steps " << extra_steps << " G0 " << G0 << " phi " << phi << " taupi " << taupi << " delta " << delta <<  std::endl;


    double w_nominal = Wavg;
    double product_p = 1.0;

     /* *********************************** END INIT ************************************ */

    /* *********************************** MAIN CYCLE ************************************ */

    bust=0.;

    if( alpha_pi < ZERO )
    {
        pi_target = 0.0;
        tau_tar   = 0.0;
    }

    int true_end = 0;



    for(t=0;t<tsim;t++){

        //renormalize in unit of price
        if(renorm == 1){
            for(i=0;i<N;i++){
                P[i]/=Pavg;
                W[i]/=Pavg;
                A[i]/=Pavg;
                PROFITS[i] /= Pavg;
            }

            S    /= Pavg;
            Wavg /= Pavg;
            Wmax /= Pavg;
            Pold /= Pavg;
            M0 /= Pavg;
            Pavg  = 1.;

        }

        /* *********************************** UPDATE ************************************ */

        pi_avg = taupi*inflation + (1.-taupi)*pi_avg;
        rp_avg = taupi*rhop + (1.-taupi)*rp_avg;
        rm_avg = taupi*rhom + (1.-taupi)*rm_avg;
        u_avg = taupi*u + (1.-taupi)*u_avg;

        //update firms variables
        Wtot = 0.;
        Ytot = 0.;
        tmp  = 0.;
        Pmin = 1.e+300 ;



        // Boltzmann average for updating unemployment share
        if(beta>0.){
            wage_norm = 0.;
            for(i=0;i<N;i++)if(ALIVE[i]==1){
                arg = beta*(W[i]-Wmax)/Wavg ;
                if(arg > -100.) wage_norm += std::exp(arg);
            }
        }
        new_len = 0 ;
        deftot = 0.;
        firm_savings = 0.;
        debt_tot = 0.;

        double pi_used = tau_tar * pi_target + tau_meas * pi_avg ;
        double frag_avg = 0.0; // Average fragility
        Gamma = std::max(alpha_g * (rm_avg-pi_used),Gamma0); //set Gamma
        for(i=0;i<N;i++){

            // living firms
            if(ALIVE[i]==1){
                pay_roll = (Y[i]*W[i]/zeta) ; //Denominator for most quantities here

                // if not bankrupt update price / production / wages and compute interests / savings and debt
                 //frag_avg += (-1.*A[i]/pay_roll);
                if((A[i] > -theta*pay_roll)||(theta<0.)){

                    frag_avg += (-1.*A[i]*Y[i]/pay_roll);
                    if(pay_roll>0.){
                        ren = Gamma * A[i] / pay_roll;
                    }  //$\Gamma \Epsilon_{i}/(W_{i} Y_{i})
                    else{
                        ren = 0.;
                    }

                    if (ren> 1.) ren=  1. ;
                    if (ren < -1.) ren= -1. ;

                    rp = gammap*dis(gen); // $\gamma_{p} \xi_{i}(t)$
                    rw = gammaw*dis(gen); // $\gamma_{w} \xi_{t}(t)$

                    dY = D[i] - Y[i] ;
                    p  = P[i];

                    if(beta>0.){
                        arg = beta*(W[i]-Wmax)/Wavg ;
                        u_share = 0.;
                        if(arg > -100.)u_share = u * N * (1.-bust) * std::exp(arg) / wage_norm;//Eqn(A9)
                    }

                    else{
                        u_share = u;
                    }

                    //excess demand
                    if(dY>0.){

                        //increase production
                        double eta = eta0p*(1.+ren); //Eqn(10) from Paper2
                        if(eta<0.0){
                            eta=0.0;
                        }// Clipping values between [0,1]
                        if(eta>1.0){
                            eta=1.0;
                        }
                        etaplus[i] = eta; // store this firms eta_{+}
                        Y[i] += std::min(eta*dY,u_share*zeta); // Eqn (9) -> Excess demand paper2

                        //increase price
                        if(p<Pavg){
                            P[i] *= (1. + rp) ; // Eqn(A8) -> top bit. from paper 1
                        }
                        //increase wage
                        if((PROFITS[i]>0.)&&(gammaw>ZERO)){
                            // why do you have gamma_w > 0 condition
                            W[i] *= 1. + (1.0+ren) * rw * e; //Eqn(A11) top bit. Sign change because we compute the fragility factor without the negative sign.

                            W[i] = std::min(W[i], zeta*(P[i]*std::min(D[i], Y[i]) + rhom*std::min(A[i],0.) + rhop*std::max(A[i],0.0))/(Y[i]));// Set wages so that profits never go to zero even with this new wage.
                            W[i] = std::max(W[i],0.); // Wages can never be zero.
                        }
                    }

                    //excess production
                    else {

                        //decrease production
                        double eta = eta0m*(1.-ren);// Eqn(10) paper2
                        if(eta<0.0){
                            eta=0.0;// Clipping values between [0,1]
                        }
                        if(eta>1.0){
                            eta=1.0;
                        }
                        etaminus[i] = eta; // Store this firms eta_{-}
                        Y[i] += eta*dY ; // Eqn 9 Paper2

                        //decrease price
                        if(p>Pavg){
                            P[i] *= (1. - rp) ; //Eqn 12 (lower bit)
                        }
                        //decrease wage
                        if(PROFITS[i]<0.){
                            W[i] *= 1. - (1.0-ren)*rw*u; // Eqn (A11) lower bit
                            W[i] = std::max(W[i],0.);
                        }
                    }

                    if(DOANTICIPATE){
                        P[i] *= 1.0 + pi_used; //set inflation expectations
                        W[i] *= 1.0 + wage_factor * pi_used; // set inflation expectations
                    }

                    Y[i] = std::max(Y[i],0.);

                    Wtot += W[i]*Y[i];
                    tmp  += P[i]*Y[i];
                    Ytot += Y[i];

                    firm_savings += std::max(A[i],0.); // Eqn (A7) paper1
                    debt_tot     -= std::min(A[i],0.); // Eqn (A7) paper 1

                    Pmin = std::min(Pmin,P[i]);

                    if((P[i]>1.0/ZERO)||(P[i]<ZERO)){
                        printf("price under/overflow... (1)\n");
                        if(P[i]>1.0/ZERO)collapse=3;
                               if (P[i]<ZERO) collapse=4;
                        t=tsim;
                    }
                }

                // if bankrupt shut down and compute default costs
                else { // This company was alive in the previous time-step but has gone bankrupt now
                    deftot -= A[i]; // Eqn(A6) Paper1
                    Y[i] = 0.;
                    ALIVE[i] = 0;
                    A[i] = 0.;
                    new_list[new_len]=i;
                    new_len++;
                }
            }
            // for companies already dead
            else{
                new_list[new_len]=i;
                new_len++;
            }
        }

        Pavg = tmp / Ytot ; // Average price pbar = p_{i} Y_{i} /(\sum_{i} Y_{i})
        Wavg = Wtot / Ytot ; // Average wage = W_{i} Y_{i}/(sum_{i} Y_{i})

        e = Ytot / N ;//Computing employment
        e /= zeta;

        u = 1. - e ; // Unemployment


        /* *********************************** INTERESTS ************************************ */

        double left = 0.0;
        if (fabs(S + firm_savings - deftot - debt_tot - M0) > 0.0){

            left = S + firm_savings - deftot - debt_tot - M0;
            if ( fabs(left) > (S+firm_savings)*ZERO ){
                std::cout << "Huge problem" << std::endl;
                std::cout << t << "\t" << S+firm_savings << "\t" << deftot <<  "\t" << debt_tot << "\t" << S + firm_savings - deftot - debt_tot << "\t" << left <<  std::endl ;
                 exit(1);
            }

            else{
                S -= left;
            }

            if (S < 0.0){
                std::cout << "Negative Savings" << std::endl;
                exit(1);
            }
        }


        double temp_rhom;
        double temp_rhop;

        temp_rhom = rho;
        if(debt_tot>0.)temp_rhom += (1.-f)*deftot / debt_tot ; // Change f -> 1-f in Eqn(7) Paper1

        interests = temp_rhom*debt_tot ;

        temp_rhop = k = 0.;
        if( S + firm_savings > 0. ){
            temp_rhop = (interests - deftot) / ( S + firm_savings ); // From Eqn(8) Paper1
            k = debt_tot / ( S + firm_savings) ;
        }

        rhop = temp_rhop;
        rhom = temp_rhom;
        S += rhop*S;
        /* ******************************* SHOCK HAPPENS HERE ********************************* */

        propensity = G * (1.+ alpha*(pi_used-rp_avg) ) ; // Eqn(5) paper 2
        propensity = std::max(propensity,0.); // Clipping values between [0,1]
        propensity = std::min(propensity,1.);

        if(shock_details.shock.shock_array[t])
        {
            std::cout << "Consumption shock" << std::endl;
            propensity = propensity*cfactor;
            std::cout << t << " propensity " << propensity << " " << G << " " << alpha << std::endl;

            switch(shock_details.shocktype)
            {
                case production:
                case prod_debt:
                    std::cout << "Production shock" << std::endl;
                    zeta = zfactor*zeta0;
                    for (int i =0; i< Y.size(); i++){
                        Y[i] *= zfactor;
                    }
                    Ytot *= zfactor;
                    std::cout << t << " Current zeta " << zeta << " Old zeta " << zeta0 << std::endl;
                    break;
                default:
                    break;
            }
        }
        else{

            for (int i = 0; i < Y.size(); i++){
                Y[i] *= zeta0/zeta;
            }
            Ytot *= zeta0/zeta;
            zeta = zeta0;
        }

        if (t>= t_end && t <= t_end+extra_steps && extra_cons == 1){
            std::cout << "Increasing consumption" << std::endl;
            propensity = propensity+0.2;
            propensity = std::min(propensity, 1.0); 
            std::cout << t << " propensity " << propensity << " " << G << " " << alpha << std::endl;
        }


        if (debt_policy.shock_array[t]){
            true_end = t;
            if (adapt == 1){
                if (t <= policy_end){
                    theta = theta0*100;
                    std::cout << t << " Theta =  " << theta << std::endl;
                    debt_policy.shock_array[t+1] = true;
                }
                else if (ptol*(frag_avg/Ytot) > theta0 ){
                    theta = ptol*frag_avg/Ytot; 
                    std::cout << t << "Theta = " << theta << "frag-avg = " << frag_avg/Ytot << std::endl;
                    debt_policy.shock_array[t+1] = true;
                }
                else{
                    theta = theta0;
                    std::cout << t << "Theta = " << theta << std::endl;
                }
            }
            else{
                std::cout << "Naive policy being applied" << std::endl;
                theta = theta0*100;
                std::cout << t << "Theta = " << theta << std::endl;
            }
        }

        else{
            theta = theta0;
        }


    if (t ==t_end){
        if (helico ==1 & shockflag > 0){
            std::cout << t << " Perform Helicopter drop" << std::endl;
            std::cout << "Increasing Savings of Households" << std::endl;
            double S0 = S;
            S += 0.5*S0;
            M0 +=  0.5*S0;
        }
    }


    /* *********************************** CONSUMPTION ************************************ */

        budget = propensity * ( Wtot/zeta + std::max(S,0.) ); // Eqn (5) Paper 2

        Pnorm = 0.; // Boltzmann average for price. Used in eqn (A3) paper 1
        for(i=0;i<N;i++)if(ALIVE[i]==1){
            arg = beta*(Pmin-P[i]) / Pavg ;
            if(arg > -100.) Pnorm += std::exp(arg);
        }

        Dtot = 0.;
        profits = 0.;
        firm_savings = 0.;

        for(i=0;i<N;i++)if(ALIVE[i]==1){

            D[i] = 0.;

            arg = beta*(Pmin-P[i])/Pavg ;

            if(arg > -100.) D[i] = budget * std::exp(arg) / Pnorm / P[i]; //Eqn (A3) Paper 1

            PROFITS[i]  = P[i]*std::min(Y[i],D[i]) - (Y[i]*W[i]/zeta) + rhom*std::min(A[i],0.) + rhop*std::max(A[i], 0.);


            // Eqn (A12) Paper 1
            S          -= P[i]*std::min(Y[i],D[i]) - (Y[i]*W[i]/zeta); // Eqn (A14) without dividend term
            A[i]       += PROFITS[i]; // Eqn (A13) without dividend term


            //Dividend payments
            if((A[i]>0.)&&(PROFITS[i]>0.)){
                //Dividends paid as a fraction of the profits
                if(dividends=='P'){
                    S    += delta*PROFITS[i];
                    A[i] -= delta*PROFITS[i];
                }
                //Dividends paid as a fraction of the cash balance
                if(dividends=='A'){
                    S    += delta*A[i];
                    A[i] -= delta*A[i];
                }
            }

            Dtot    += D[i];
            profits += PROFITS[i];

            firm_savings += std::max(A[i],0.);

            }
        // Beyond this point there is only the revival bit and nothing else
        // So we define the u-rate and bust here
        //
        double u_bef = u;
        double bust_bef = (N - sum_vector(ALIVE))/N;

        /* ******************************* REVIVAL ******************************** */

        //revival
        deftot = 0.;
        for(i=0;i<new_len;i++)if(dis(gen)<phi){

            new_firm = new_list[i];
            Y[new_firm] = std::max(u,0.)*dis(gen);
            D[new_firm] = Y[new_firm];
            etaplus[i]  = eta0p;
            etaminus[i] = eta0m; 
            ALIVE[new_firm] = 1;
            if (u_bef == 1 ){
                Pavg = 1.0;
                Wavg = 1.0;
                P[new_firm] = Pavg;
                W[new_firm] = Wavg;
                A[new_firm] = 0.0;
            }
            else{
                P[new_firm] = Pavg;
                W[new_firm] = Wavg;
                A[new_firm] = (W[new_firm]*Y[new_firm]);
            }

            deftot  += A[new_firm];

            firm_savings += A[new_firm];

            PROFITS[new_firm] = 0.;
            }



        /* ******************************* FINAL ******************************** */

        //new averages
        tmp  = 0.;
        Ytot = 0.;
        Wtot = 0.;
        bust = 0.;
        Wmax = 0.;
        Atot = 0.;
        double etaplus_avg = 0.0;
        double etaminus_avg = 0.0;
        debt_tot = 0.;
        int firms_alive = 0;


        for(i=0;i<N;i++){

            //final averages
            if(ALIVE[i]==1){

                if((firm_savings>0.)&&(A[i]>0.))A[i] -= deftot*A[i]/firm_savings;

                Wtot    += Y[i]*W[i];
                Ytot    += Y[i];
                tmp     += P[i]*Y[i];

                Wmax = std::max(W[i],Wmax);

                debt_tot -= std::min(A[i],0.);
                Atot     += A[i];

                etaplus_avg += etaplus[i];
                etaminus_avg += etaminus[i];
                firms_alive +=1;

            }

            else bust += 1./N;

        }

        Pavg = tmp / Ytot;
        Wavg = Wtot / Ytot;

        inflation = (Pavg-Pold)/Pold;
        Pold = Pavg;

        e = Ytot / N  ;

        e /=zeta; 
        if((e-1 > ZERO)|| (e < -ZERO) || (S < 0.0) ){
            printf("Error!! -> t =%d\t e = %.10e\tS=%.10e\n",t+1,e,S);
            std::cout << "Zeta here " << zeta << " Propensity " << propensity << " Atot " << Atot << " firm savings " << firm_savings << std::endl;
            collapse=2;
            t=tsim;
        }
        //e /= zeta;
        e = std::min(e,1.);
        e = std::max(e,0.);

        u =  1. - e;

        if(Ytot<ZERO){
            printf("Collapse\n");
            collapse = 1;
            t=tsim;
        }

        /************************* INTEREST RATE SETTING ************************************/

        rho = rho0 + alpha_pi * ( pi_avg - pi_target ) ;

        /************************************** OUTPUT ************************************/


        if((t%tprint==0)){
            double R_temp = eta0p/eta0m;
            // Implementing the output allows for easily adding other variables to
            // be saved into output vector.
            output_vector = Vector{static_cast<double>(t), u_bef, bust_bef, Pavg, Wavg, S, Atot, firm_savings, debt_tot, inflation, pi_avg, propensity, k, Dtot, rhom, rho, rhop, pi_used, tau_tar, tau_meas, R_temp};

            output_vector.push_back(Wtot);
            output_vector.push_back(etaplus_avg/firms_alive);
            output_vector.push_back(etaminus_avg/firms_alive);
            output_vector.push_back(w_nominal);
            output_vector.push_back(Ytot);
            output_vector.push_back(deftot);
            output_vector.push_back(profits);
            output_vector.push_back(debt_tot/S);
            output_vector.push_back(static_cast<double>(firms_alive));
            output_vector.push_back(left);
            output_vector.push_back(u);
            output_vector.push_back(bust);
            output_vector.push_back(frag_avg/Ytot);
            output_vector.push_back(true_end);
            output_vector.push_back(theta);


            output.write_vector_to_file(output_vector);

            output_vector.clear();
        }

    }


    output.close_file(); 

    return 0; 
}
